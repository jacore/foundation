package com.base.interview;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目启动类
 * @author X
 */
@MapperScan("com.base.interview.**.dao")
@SpringBootApplication
public class SpingApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpingApplication.class, args);
    }
}
