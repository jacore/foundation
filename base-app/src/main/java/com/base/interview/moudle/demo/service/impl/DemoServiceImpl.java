package com.base.interview.moudle.demo.service.impl;

import com.base.interview.moudle.demo.dao.DemoMapper;
import com.base.interview.moudle.demo.service.DemoService;
import  com.base.interview.moudle.demo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class DemoServiceImpl implements DemoService {

    @Autowired
    private DemoMapper dao;


    public List<Map<String, Object>> demoSelet() {
        return dao.queryTest();
    }
}
