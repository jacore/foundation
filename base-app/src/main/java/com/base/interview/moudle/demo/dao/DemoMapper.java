package com.base.interview.moudle.demo.dao;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Repository
public interface DemoMapper {
    List<Map<String,Object>> queryTest();
}
