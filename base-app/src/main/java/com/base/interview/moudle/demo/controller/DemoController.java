package com.base.interview.moudle.demo.controller;

import com.base.interview.moudle.demo.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class DemoController {
    @Autowired
    DemoService service;

    @RequestMapping("/demo")
    public String demoTest(){
        List<Map<String, Object>> result = service.demoSelet();
        System.out.println(result);
        return result.toString();
    }
}
